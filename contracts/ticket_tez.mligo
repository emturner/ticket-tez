(* SPDX-CopyrightText Trilitech <contact@trili.tech> *)

#include "./ticket_type.mligo"

type storage = unit

type parameter =
  Mint of tez_ticket contract
| Burn of (unit contract * tez_ticket)

type return = operation list * storage

let mint contract : return =
  let amount: nat = Tezos.get_amount () / 1mutez in
  let tickets = match (Tezos.create_ticket (0n, (None: bytes option)) amount) with
  | Some t -> t
  | None -> failwith "Could not mint ticket." in
  ([Tezos.transaction tickets 0mutez contract], ())

let burn contract (ticket: tez_ticket) : return =
  if Tezos.get_amount () > 0tez then
    failwith "Burn does not accept tez."
  else
  let (addr, (_, amt)), _ticket = Tezos.read_ticket ticket in
  if addr <> (Tezos.get_self_address ()) then failwith "Burn only accepts tez tickets."
  else
  let amount: tez = amt * 1mutez in
  ([Tezos.transaction () amount contract], ())
// Two entrypoints

(* Main access point that dispatches to the entrypoints according to
   the smart contract parameter. *)

let main (action, _store : parameter * storage) : return =
  match action with
  | Mint callback -> mint callback
  | Burn (callback, tt) -> burn callback tt
