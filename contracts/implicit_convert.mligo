(* SPDX-CopyrightText 2023 Trilitech <contact@trili.tech> *)

#include "./ticket_type.mligo"

type storage = address (* address of TT contract *)

type parameter =
| Mint of unit
| Burn of tez_ticket

type return = operation list * storage

let mint tt_address : return =
  let amount = Tezos.get_amount () in
  let callback: tez_ticket contract = Tezos.get_contract_with_error (Tezos.get_source()) "Oops" in
  match Tezos.get_entrypoint_opt "%mint" tt_address with
  | None -> failwith "Contract does not accept tickets."
  | Some contract -> ([Tezos.transaction callback amount contract], tt_address)

let burn (ticket: tez_ticket) tt_address : return =
  if Tezos.get_amount () > 0tez then
    failwith "Burn does not accept tez."
  else
  let callback: unit contract = Tezos.get_contract_with_error (Tezos.get_source()) "Oops" in
  match Tezos.get_entrypoint_opt "%burn" tt_address with
  | None -> failwith "Contract does not accept only tez."
  | Some contract -> ([Tezos.transaction (callback, ticket) 0tez contract], tt_address)
// Two entrypoints

(* Main access point that dispatches to the entrypoints according to
   the smart contract parameter. *)

let main (action, tt_address : parameter * storage) : return =
  match action with
  | Mint () -> mint tt_address
  | Burn t -> burn t tt_address
