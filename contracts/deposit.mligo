(* SPDX-CopyrightText 2023 Trilitech <contact@trili.tech> *)

#include "./ticket_type.mligo"

type deposit =
((bytes (* L2 Address *)
 * tez_ticket)
* (int (* max gas price *)
 * bytes (* unknown - maybe for future? *)
))

type request_deposit = {
    l2_address: bytes; (* L2 address *)
    max_gas_price: int; (* Max gas price *)
    other: bytes; (* Further input *)
}

type storage = {
    tt_address: address; (* address of TT contract *)
    sr_address: address; (* Smart Rollup address *)
    request_deposit: request_deposit option; (* address of L2 depositee *)
}

type parameter =
| Deposit of request_deposit
| Callback of tez_ticket

type return = operation list * storage

let deposit request ({tt_address; sr_address; request_deposit}: storage) : return =
  let () = (match request_deposit with
  | None -> ()
  | Some _ -> failwith "deposit locked") in
  let amount = Tezos.get_amount () in
  let callback: tez_ticket contract = Tezos.self("%callback") in
  match Tezos.get_entrypoint_opt "%mint" tt_address with
  | None -> failwith "Contract does not accept tickets."
  | Some contract -> ([Tezos.transaction callback amount contract], {tt_address; sr_address; request_deposit = Some request})

let callback (ticket: tez_ticket) {tt_address; sr_address; request_deposit} : return =
  let request_deposit = (match request_deposit with
  | None -> failwith "Callback on non-locked deposit"
  | Some r -> r) in
  let sr: deposit contract = Tezos.get_contract_with_error sr_address "Oops - bad rollup address" in
  let {l2_address; max_gas_price; other} = request_deposit in
  let deposit: deposit = ((l2_address, ticket), (max_gas_price, other)) in
  ([Tezos.transaction deposit 0tez sr], {tt_address; sr_address; request_deposit = None})

let main (action, storage : parameter * storage) : return =
  match action with
  | Deposit request -> deposit request storage
  | Callback tez_ticket -> callback tez_ticket storage
