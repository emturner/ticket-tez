(* SPDX-CopyrightText Trilitech <contact@trili.tech> *)

type tez_ticket = (nat * bytes option) ticket
