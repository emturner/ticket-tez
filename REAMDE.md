# Ticket Tez (ꜩT)

**Ticket Tez** is a simple contract designed for converting between Tez and a wrapped version of Tez as a ticket.

Contracts for **Ticket Tez** are placed in `contracts/`. They can be compiled with:

```sh
alias ligo='docker run --rm -v "$PWD":"$PWD" -w "$PWD" ligolang/ligo:0.71.1'

ligo compile contract ./contracts/<contract>.mligo --protocol nairobi
```

## ticket_tez.mligo

This contract does the conversion between **tez** and **Ticket Tez**. Two entrypoints are provided, `%mint` and `%burn`.

`%mint` consumes ꜩ, and will call the given contract with the equivalent ꜩT. Note that **1ꜩT is equivalent to one mutez**.

## implicit_convert.mligo

This contract allows easier conversion between ꜩ & ꜩT for implicit accounts. It should be originated with the address of *ticket_tez* contract.

```
octez-client transfer 1 from bob to <implicit_convert> --entrypoint mint
```

Will put 1million ꜩT into bob's account. 1ꜩ will be transferred to the <ticket_tez> contract. You can check with:

```
octez-client get ticket balance for bob with ticketer <ticket_tez> and type 'pair nat (option bytes)' and content 'Pair 0 None'
```

To recover the original tez, run:

```
octez-client transfer 1000000 tickets from bob to <implicit_convert> with entrypoint burn and contents 'Pair 0 None' and type 'pair nat (option bytes)' and ticketer <ticket_tez>
```

## deposit.mligo

This is designed to be used for depositing ꜩT directly into a smart rollup (potentially EVM).

NB this should be originated with the <ticket_tez> contract, the target rollup address, and `None`.
